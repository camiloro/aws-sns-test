package com.example.sns.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.example.sns.dto.ContentMessage;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class SnsPublisherService implements PublisherService{

	private AmazonSNS amazonSNS;
    private String snsTopicDemoARN;
    private Logger logger = LogManager.getLogger(Logger.class.getName());
    
    @Autowired
    public SnsPublisherService(BasicAWSCredentials sessionCredentials, String snsTopicDemoARN) {

        this.amazonSNS = AmazonSNSClientBuilder.standard().withRegion(Regions.US_EAST_2).withCredentials(new AWSStaticCredentialsProvider(sessionCredentials)).build();
        this.snsTopicDemoARN = snsTopicDemoARN;

    }
	
	@Override
	public PublishResult publish(ContentMessage message) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String notificationJson = mapper.writeValueAsString(message);
		
		logger.info("Json to publish :" + notificationJson + " in topic: "+this.snsTopicDemoARN);
		
		PublishRequest publishRequest = new PublishRequest(this.snsTopicDemoARN, notificationJson, message.getDescription());
        PublishResult publishResult = this.amazonSNS.publish(publishRequest);
        
        logger.info("Message Published in topic: "+this.snsTopicDemoARN+" with response: "+publishResult.getMessageId());
        
        return publishResult;
		
	}

}
