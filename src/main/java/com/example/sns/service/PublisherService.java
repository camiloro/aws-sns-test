package com.example.sns.service;

import com.amazonaws.services.sns.model.PublishResult;
import com.example.sns.dto.ContentMessage;

public interface PublisherService {
	
	PublishResult publish(ContentMessage message) throws Exception;

}
