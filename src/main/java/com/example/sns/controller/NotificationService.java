package com.example.sns.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.services.sns.model.PublishResult;
import com.example.sns.dto.ContentMessage;
import com.example.sns.exception.ResourceNotFoundException;
import com.example.sns.service.PublisherService;
import com.example.sns.service.builder.NotificationBuilder;

@RestController
@RequestMapping("/")
public class NotificationService {

	private Logger logger = LogManager.getLogger(Logger.class.getName());
	
	@Autowired
	private PublisherService publisherService;

	@GetMapping(path="/id/{id}")
	@ResponseStatus(HttpStatus.OK)
	public PublishResult publishNotification(@PathVariable  String id) {

		try {
			ContentMessage messageToSend= NotificationBuilder.buildMessage(id);
			return publisherService.publish(messageToSend);
		} catch (Exception e) {
			logger.error("ERROR Service aws-sns-service has failed. Request with id: "+id+ "exception detail is: "+ e);
			throw new ResourceNotFoundException("Error executing request with id: "+id);
		}

	}

}
