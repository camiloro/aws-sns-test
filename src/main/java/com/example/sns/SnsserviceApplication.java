package com.example.sns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication
public class SnsserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SnsserviceApplication.class, args);
	}

}
