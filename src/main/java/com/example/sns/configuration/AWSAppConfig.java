package com.example.sns.configuration;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
@PropertySource("classpath:application.properties")
public class AWSAppConfig {

	@Value("${aws.sns.topic.demo.ARN}")
	String snsTopicDemoARN;

	@Bean(name = "snsTopicDemoARN")
	public String snsTopcARN() {
		return this.snsTopicDemoARN;
	}


	@Bean(name = "sessionCredentials")
	public BasicAWSCredentials sessionCredentials() {
  
        BasicAWSCredentials sessionCredentials = (BasicAWSCredentials) new EnvironmentVariableCredentialsProvider().getCredentials();
        return sessionCredentials;
	}

}
